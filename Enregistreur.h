#ifndef __Enregistreur_h__
#define __Enregistreur_h__

#include "Arduino.h"

class Enregistreur {
  private:
    int8_t mEchantillons[256];
    byte mIndex;
    bool mEnregistre;
    
  public:
    Enregistreur() : mIndex(0), mEnregistre(false) {}
    
    /* Ajoute un échantillon, stoppe si le tampon déborde */
    void ajoute(int echantillon) {
      if (mEnregistre) {
        if (echantillon > 127) echantillon = 127;
        else if (echantillon < -128) echantillon = -128;
        mEchantillons[mIndex++] = echantillon;
        if (mIndex == 0) mEnregistre = false;
      }
    }
    
    /* Démarre l'enregistrement */
    void demarre() { mEnregistre = true; }
    /* Vrai si aucun enregistrement n'est en cours */
    bool fini()    { return ! mEnregistre; }
    
    void affiche(HardwareSerial &serial, char separateur);
};

// extern Enregistreur enregistreurErreur;

#endif
