#ifndef __QuickPort_h__
#define __QuickPort_h__

class QuickPort {
  private:
    byte mBit;
    byte mPort;
  
  public:
    QuickPort(byte pin) : mBit(digitalPinToBitMask(pin)), mPort(digitalPinToPort(pin)) {}
    
    void setOutput() {
      volatile uint8_t *reg = portModeRegister(mPort);
      *reg |= mBit;
    }
    
    void setInput() {
      volatile uint8_t *reg = portModeRegister(mPort);
      *reg &= ~mBit;
    }
    
    void writeLOW() {
      volatile uint8_t *out = portOutputRegister(mPort);
      *out &= ~mBit;
    }
    
    void writeHIGH() {
      volatile uint8_t *out = portOutputRegister(mPort);
      *out |= mBit;
    }
    
    void write(byte value) {
      if (value) writeHIGH();
      else writeLOW();
    }
    
    byte read() {
      return ((*portInputRegister(mPort) & mBit) != 0);
    }
};

#endif
