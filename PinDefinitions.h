/*
 * Alimentation Traction Asservie
 *
 * Définition des broches employées sur la carte
 */

#ifndef __DefinitionDesBroches__
#define __DefinitionDesBroches__

#include "Arduino.h"

/*
 * Interruption du MCP2515
 */
static const byte pinInterruptionCAN = 2;

/*
 * Entrée : Poussoir pour mettre la carte en mode configuration
 */
static const byte pinConfiguration = 4;

/*
 * Sortie : Commande de court-circuitage de la détection afin
 * de mesurer la FCEM
 */
static const byte pinActiverShuntDetection = 5;

/*
 * Sortie : Activation de la détection lorque la PWM est à 0
 */
static const byte pinActiverDetection = A2;

/*
 * Sortie : Commande relai de sens de circulation
 */
static const byte pinRelai = A3;

/*
 * Entrée : Détection de présence en pleine voie
 */
static const byte pinDetectionPleineVoie = A4;

/*
 * Entrée : Détection de présence en zone d'arret
 */
static const byte pinDetectionZoneArret = A5;

/*
 * Entrée : Mesure de vitesse
 */
static const byte pinVitesse = A6;

/*
 * Entrée : Mesure de courant
 */
static const byte pinCourant = A7;

/*
 * Entrée : Détection de court-circuit
 */
static const byte pinDetectionCourtCircuit = 7;

/*
 * Entrée : Signal de synchronisation des PWM
 */
static const byte pinSynchroPWM = 8;

/*
 * Entrée : signal de synchronisation de la mesure de vitesse
 */
static const byte pinSychroVitesse = 9;

/*
 * Sortie : PWM
 */
static const byte pinPWM = 3;

/*
 * Entrée / Sortie : LEDs du charlieplexing
 */
static const byte pinLED0 = 6;
static const byte pinLED1 = A0;
static const byte pinLED2 = A1;

#endif
