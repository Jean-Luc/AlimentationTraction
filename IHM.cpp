#include <avr/pgmspace.h>
#include "IHM.h"
#include "PinDefinitions.h"

enum { OUT0, OUT1, INZ };

typedef struct {
  byte etatLED0 : 2;
  byte etatLED1 : 2;
  byte etatLED2 : 2;
} CombinaisonLED;

static const CombinaisonLED combi[6] = {
  { OUT0, OUT1, INZ  }, /* CAN_RX_LED        */
  { INZ,  OUT0, OUT1 }, /* CAN_TX_LED        */
  { OUT1, INZ,  OUT0 }, /* BLINK_LED         */
  { OUT0, INZ,  OUT1 }, /* DETECTION_PV_LED  */
  { OUT1, OUT0, INZ  }, /* DETECTION_AR_LED  */
  { INZ,  OUT1, OUT0 }  /* COURT_CIRCUIT_LED */
};

static const char rxName[] PROGMEM    = "rx";
static const char txName[] PROGMEM    = "tx";
static const char blinkName[] PROGMEM = "blk";
static const char dpvName[] PROGMEM   = "dpv";
static const char darName[] PROGMEM   = "dar";
static const char ccName[] PROGMEM    = "cc";

static const char * const nameTable[] PROGMEM = {
  rxName,
  txName,
  blinkName,
  dpvName,
  darName,
  ccName
};

static char buffer[6];

/*
 * Constructeur. Toutes les LED sont éteintes, l'index est initialisé à 0
 */
CharlieLED::CharlieLED()
  : mIndex(0),
    mLastDate(0)
{
  for (byte led = 0; led < 6; led++) {
    mAllume[led] = false;
  }
}

/*
 * Cycle d'allumage. update doit etre appelée dans loop
 */
void CharlieLED::maj()
{
  unsigned long date = millis();
  if (date - mLastDate >= 1) {
    mLastDate = date;
    
    byte dirC = DDRC & B11111100;
    byte dirD = DDRD & B10111111;
    byte dataC = PORTC;
    byte dataD = PORTD;
    
    if (mAllume[mIndex]) {
      switch (combi[mIndex].etatLED0) {
        case OUT0 : dataD &= ~(1 << PD6); dirD |=  (1 << PD6); break;
        case OUT1 : dataD |=  (1 << PD6); dirD |=  (1 << PD6); break;
        case INZ  : dataD &= ~(1 << PD6); dirD &= ~(1 << PD6);
      }
      switch (combi[mIndex].etatLED1) {
        case OUT0 : dataC &= ~(1 << PC0); dirC |=  (1 << PC0); break;
        case OUT1 : dataC |=  (1 << PC0); dirC |=  (1 << PC0); break;
        case INZ  : dataC &= ~(1 << PC0); dirC &= ~(1 << PC0);
      }
      switch (combi[mIndex].etatLED2) {
        case OUT0 : dataC &= ~(1 << PC1); dirC |=  (1 << PC1); break;
        case OUT1 : dataC |=  (1 << PC1); dirC |=  (1 << PC1); break;
        case INZ  : dataC &= ~(1 << PC1); dirC &= ~(1 << PC1);
      }
    }
    
    PORTC = dataC;
    DDRC = dirC;
    PORTD = dataD;
    DDRD = dirD;
    
    mIndex++;
    if (mIndex == 6) mIndex = 0;
//    
//    Serial.print("DDRC  = "); Serial.println(dirC, BIN);
//    Serial.print("PORTC = "); Serial.println(dataC, BIN);
//    Serial.print("DDRD  = "); Serial.println(dirD, BIN);
//    Serial.print("PORTD = "); Serial.println(dataD, BIN);
  }
}

void CharlieLED::afficheEtat(HardwareSerial &serial)
{
  for (byte led = 0; led < 6; led++) {
    strcpy_P(buffer, (char*)pgm_read_word(&(nameTable[led])));
    serial.print(buffer);
    for (byte i = strlen(buffer); i < 3; i++) serial.write(' ');
    serial.write(':');
    serial.write(' ');
    if (mAllume[led]) serial.println(F("on"));
    else serial.println(F("off"));
  }
}

int8_t CharlieLED::nomExiste(char *name)
{
  int8_t result = -1;
  for (byte led = 0; led < 6; led++) {
    strcpy_P(buffer, (char*)pgm_read_word(&(nameTable[led])));
    if (strcmp(name, buffer) == 0) {
      result = led;
      break;
    }
  }
  return result;
}


CharlieLED leds;

